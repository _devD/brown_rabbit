// image gallerry js
const photoList = [
  "Post thumbnail-1.png",
  "Post thumbnail-2.png",
  "Post thumbnail-3.png",
];

const bigImg = document.querySelector("#mainPhoto");
const thumbArray = Array.from[photoList];
let index = 0;
let newPhoto;

document.querySelector("#next").addEventListener("click", change);
document.querySelector("#back").addEventListener("click", change);

function change() {
  if (this.getAttribute("id") === "next") {
    if (index < photoList.length - 1) {
      index++;
    }
  } else {
    if (index > 0) {
      index--;
    }
  }
  newPhoto = "./assets/images/posts/" + photoList[index];
  bigImg.setAttribute("src", newPhoto);

  if (index === 0) {
    back.style.display = "none";
  } else if (index === photoList.length - 1) {
    next.style.display = "none";
  } else {
    back.style.display = "block";
    next.style.display = "block";
  }
}

// read more and read less js

document.querySelectorAll(".card-button").forEach((btn) => {
  btn.addEventListener("click", (event) => {
    let e = event.target.previousElementSibling;
    if (e.classList.contains("detail")) {
      if (e.style.display !== "block") {
        e.style.display = "block";
        event.target.textContent = "Read less";
      } else {
        e.style.display = "none";
        event.target.textContent = "Read more";
      }
    }
  });
});

/* const text = document.querySelector("#moreInfo");
const button = document.querySelector("#moreButton");

button.addEventListener("click", showHide);

function showHide() {
  if (text.style.display !== "block") {
    text.style.display = "block";
    button.textContent = "Read Less";
  } else {
    text.style.display = "none";
    button.textContent = "Read More";
  }
} */

/* button.addEventListener("click", showHide);
function showHide() {
  document.querySelectorAll("#moreButton").forEach((btn) => {
    btn.addEventListener("click", (event) => {
      let e = event.target.previousElementSibling;
      if (e.classList.contains("#moreInfo")) {
        if (e.style.display !== "block") {
          e.style.display = "block";
          event.target.textContent = "Read less";
        } else {
          e.style.display = "none";
          event.target.textContent = "Read more";
        }
      }
    });
  });
} */
